module gitlab.infra.com/devopsman/jx-demo

// +heroku goVersion go1.16
go 1.16

require (
	github.com/BurntSushi/toml v1.0.0 // indirect
	github.com/coreos/go-semver v0.3.0 // indirect
	github.com/gitcpu-io/zgo v1.0.3
	github.com/gogo/protobuf v1.3.2
	github.com/golang/protobuf v1.5.2
	github.com/kataras/iris/v12 v12.1.8
	github.com/ryanuber/columnize v2.1.2+incompatible // indirect
	github.com/spf13/pflag v1.0.5
	golang.org/x/sys v0.0.0-20210927094055-39ccf1dd6fa6 // indirect
	google.golang.org/grpc v1.43.0
	gopkg.in/ini.v1 v1.62.0 // indirect
)

replace (
	github.com/coreos/bbolt => go.etcd.io/bbolt v1.3.2
	//github.com/gitcpu-io/zgo => ../zgo
	google.golang.org/grpc v1.43.0 => github.com/grpc/grpc-go v1.26.0
)
